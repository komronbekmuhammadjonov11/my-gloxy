
import './App.css';
import "./components/home";
import Home from "./components/home";
import {BrowserRouter, Switch, Route} from "react-router-dom";


function App() {
  return (
    <div className="App">
     <BrowserRouter>
             <Route path="/" component={Home}/>
     </BrowserRouter>
    </div>
  );
}

export default App;
