import Navbar from "./navbar";
import "../style/header.scss";
import Templates from "./templates";
function Header() {
    return <div className="header" id="header">
        <Navbar/>
        <Templates/>
    </div>
}
export default Header