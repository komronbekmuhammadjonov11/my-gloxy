import "../style/features.scss";
import AOS from "aos"
function Features() {
    AOS.init()
    return <div className="features" id="features">
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <h2>Awesome Features</h2>
                    <ul className="support">
                        <li>FRIENDLY SUPPORT</li>
                        <li></li>
                        <li>POWERFUL DESIGN</li>
                    </ul>
                </div>
            </div>
            <div className="row">
                <div className="col-md-4" data-aos="flip-left" data-aos-duration={400}>
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/gatsby.png" alt="..."/>
                            </div>
                            <p>Gatsby</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-4" data-aos="flip-left" data-aos-duration={400} data-aos-delay={100} >
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/graphql.png" alt="..."/>
                            </div>
                            <p>Graphql</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-4"  data-aos="flip-left" data-aos-duration={400} data-aos-delay={200} >
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/bootstrap.png" alt="..."/>
                            </div>
                            <p>Built on Bootstrap (4.X)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-4"  data-aos="flip-left" data-aos-duration={400} data-aos-delay={300}>
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/w3c.png" alt="..."/>
                            </div>
                            <p>Valid HTML5 & CSS3 Code</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-4"  data-aos="flip-left" data-aos-duration={400} data-aos-delay={400} >
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/retina.png" alt="..."/>
                            </div>
                            <p>Clean & Professional Code
                                image
                            </p>
                        </div>
                    </div>
                </div>
                <div className="col-md-4"  data-aos="flip-left" data-aos-duration={400} data-aos-delay={500} >
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/font.png" alt="..."/>
                            </div>
                            <p>Free Google fonts & Icon Used</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-4"  data-aos="flip-left" data-aos-duration={400} data-aos-delay={600}>
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/maps.png" alt="..."/>
                            </div>
                            <p>Google Map</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-4"  data-aos="flip-left" data-aos-duration={400} data-aos-delay={700} >
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/browser.png" alt="..."/>
                            </div>
                            <p>Browser Compatibility</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-4"  data-aos="flip-left" data-aos-duration={400} data-aos-delay={800}>
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/support.png" alt="..."/>
                            </div>
                            <p>Quick Support</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-4"  data-aos="flip-left" data-aos-duration={400} data-aos-delay={900}>
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/wrench.png" alt="..."/>
                            </div>
                            <p>Easy to Customize</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-4"  data-aos="flip-left" data-aos-duration={400} data-aos-delay={1000}>
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/retina.png" alt="..."/>
                            </div>
                            <p>Retina Ready</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-4"  data-aos="flip-left" data-aos-duration={400} data-aos-delay={1100}>
                    <div className="card">
                        <div className="featuresContent">
                            <div className="imgContent">
                                <img src="images/thumbs.png" alt="..."/>
                            </div>
                            <p>Free Lifetime Updates</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
}
export default Features