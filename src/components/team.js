
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import "../style/team.scss"
import {Link} from "react-router-dom"
function Team() {
    const responsive = {
        0: {items: 1},
        576: {items: 2},
        1024: {items: 3},
    };
    const items = [
        <div className="item" data-value="1">
            <div className="teamCard">
                <img src="images/img_6.png" alt="..."/>
                <div className="teamName">
                    <h5>Davies</h5>
                    <h6>QA Expert</h6>
                    <div className="icons">
                        <Link to={"/"}>
                            <div className="iconContent">
                                <i className="fab fa-facebook-f"></i>
                            </div>
                        </Link>
                        <Link to="/">
                            <div className="iconContent">
                                <i className="fab fa-linkedin-in"></i>
                            </div>
                        </Link>
                        <Link>
                            <div className="iconContent">
                                <i className="fab fa-twitter"></i>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        </div>,
        <div className="item" data-value="2">
            <div className="teamCard">
                <img src="images/img_7.png" alt="..."/>
                <div className="teamName">
                    <h5>Williams</h5>
                    <h6>CEO & Founder</h6>
                    <div className="icons">
                        <Link to={"/"}>
                            <div className="iconContent">
                                <i className="fab fa-facebook-f"></i>
                            </div>
                        </Link>
                        <Link to="/">
                            <div className="iconContent">
                                <i className="fab fa-linkedin-in"></i>
                            </div>
                        </Link>
                        <Link>
                            <div className="iconContent">
                                <i className="fab fa-twitter"></i>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        </div>,
        <div className="item" data-value="3">
            <div className="teamCard">
                <img src="images/img_8.png" alt="..."/>
                <div className="teamName">
                    <h5>Thomas</h5>
                    <h6>Team Leader</h6>
                    <div className="icons">
                        <Link to={"/"}>
                            <div className="iconContent">
                                <i className="fab fa-facebook-f"></i>
                            </div>
                        </Link>
                        <Link to="/">
                            <div className="iconContent">
                                <i className="fab fa-linkedin-in"></i>
                            </div>
                        </Link>
                        <Link>
                            <div className="iconContent">
                                <i className="fab fa-twitter"></i>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        </div>,
        <div className="item" data-value="4">
            <div className="teamCard">
                <img src="images/img_9.png" alt="..."/>
                <div className="teamName">
                    <h5>Taylor</h5>
                    <h6>React Devoloper</h6>
                    <div className="icons">
                        <Link to={"/"}>
                            <div className="iconContent">
                                <i className="fab fa-facebook-f"></i>
                            </div>
                        </Link>
                        <Link to="/">
                            <div className="iconContent">
                                <i className="fab fa-linkedin-in"></i>
                            </div>
                        </Link>
                        <Link>
                            <div className="iconContent">
                                <i className="fab fa-twitter"></i>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        </div>,
        <div className="item" data-value="5">
            <div className="teamCard">
                <img src="images/img_10.png" alt="..."/>
                <div className="teamName">
                    <h5>Brown</h5>
                    <h6>Designer</h6>
                    <div className="icons">
                        <Link to={"/"}>
                            <div className="iconContent">
                                <i className="fab fa-facebook-f"></i>
                            </div>
                        </Link>
                        <Link to="/">
                            <div className="iconContent">
                                <i className="fab fa-linkedin-in"></i>
                            </div>
                        </Link>
                        <Link>
                            <div className="iconContent">
                                <i className="fab fa-twitter"></i>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        </div>,
    ];
    return <div className="team" id="developer">
        <div className="container">
            <div className="row">
               <div className="col-md-12">
                   <h1>Our Team</h1>
                   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti dolores doloribus mollitia nobis
                       temporibus vel.</p>
               </div>
            </div>
            <AliceCarousel
                autoPlay
                autoPlayInterval={1000}
                infinite
                mouseTracking
                items={items}
                responsive={responsive}
                controlsStrategy="alternate"
            />
        </div>
    </div>
}

export default Team