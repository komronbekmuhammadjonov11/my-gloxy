import Header from "./header";
import Demo from "./demo";
import Features from "./features";
import Team from "./team";
import Contact from "./contact";
import "../style/media.scss";
import Footer from "./footer";
function Home(){
    return <div className="home">
        <Header/>
        <Demo/>
        <Features/>
        <Team/>
        <Contact/>
        <Footer/>
    </div>
}

export default Home