import AOS from "aos";
function Templates() {
    AOS.init();
    return <div className="templates" id="temple">
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <ul className="animationUl">
                        <li className="animationLi">Responsive Layout</li>
                        <li className="animationLi">Owl Carousel</li>
                        <li className="animationLi">Developer Friendly</li>
                    </ul>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12">
                    <h1 data-aos="slide-left" data-aos-duration={900}>Gloxy - Gatsby Creative Multi-Purpose
                        <br/>Templates</h1>
                </div>
            </div>
        </div>
    </div>
}
export default Templates