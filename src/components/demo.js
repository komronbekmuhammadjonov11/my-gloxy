import "../style/demo.scss";
import AOS from "aos";


function Demo() {
    AOS.init()
    return <div className="demo">
        <div className="container-fluid">
            <div className="row" >
                <div className="col-md-12">
                    <h1>DEMO PAGE</h1>
                </div>
            </div>
            <div className="row" >
                <div className="col-md-4" data-aos="fade-right" data-aos-duration={500}>
                    <div className="card">
                        <div  className="topCard" >
                            <h3>HOMEPAGE 4</h3>
                        </div>
                        <img src="images/img.png" className="img-fluid" alt="rasm bor"/>
                        <div className="textPosition">
                            <h3>HOMEPAGE DEMO 4</h3>
                        </div>
                    </div>
                </div>
                <div className="col-md-4"  data-aos="fade-down" data-aos-duration={500}>
                    <div className="card">
                        <div  className="topCard" >
                            <h3>HOMEPAGE 1</h3>
                        </div>
                        <img src="images/img_1.png" className="img-fluid" alt="rasm bor"/>
                        <div className="textPosition">
                            <h3>HOMEPAGE DEMO 1</h3>
                        </div>
                    </div>
                </div>
                <div className="col-md-4"  data-aos="fade-left" data-aos-duration={500}>
                    <div className="card">
                        <div  className="topCard" >
                            <h3>HOMEPAGE 2</h3>
                        </div>
                        <img src="images/img_2.png" className="img-fluid" alt="rasm bor"/>
                        <div className="textPosition">
                            <h3>HOMEPAGE DEMO 2</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row" >
                <div className="col-md-4" >
                    <div className="card"  data-aos="zoom-in-right" data-aos-duration={600}>
                        <div  className="topCard" >
                            <h3>HOMEPAGE 4</h3>
                        </div>
                        <img src="images/img_3.png" className="img-fluid" alt="rasm bor"/>
                        <div className="textPosition">
                            <h3>HOMEPAGE DEMO 3</h3>
                        </div>
                    </div>
                </div>
                <div className="col-md-4" >
                    <div className="card" data-aos="zoom-in-down" data-aos-duration={600}>
                        <div  className="topCard" >
                            <h3>HOMEPAGE 1</h3>
                        </div>
                        <img src="images/img_4.png" className="img-fluid" alt="rasm bor"/>
                        <div className="textPosition">
                            <h3>BLOG PAGE</h3>
                        </div>
                    </div>
                </div>
                <div className="col-md-4" >
                    <div className="card"  data-aos="zoom-in-left" data-aos-duration={600}>
                        <div  className="topCard" >
                            <h3>HOMEPAGE 2</h3>
                        </div>
                        <img src="images/img_5.png" className="img-fluid" alt="rasm bor"/>
                        <div className="textPosition">
                            <h3>BLOG DETAILS</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default Demo