import "../style/footer.scss";
import {Link} from "react-scroll"
function Footer() {
    window.addEventListener("scroll", ()=>{
        const scrolY=window.scrollY;
        document.getElementById('dNone').classList.toggle("dBlock", scrolY>900);
    })
    return <div className="footer">
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="footerLeft">
                        <p>© 2021 All Rights Reserved.</p>
                    </div>
                    <div className="footerRight">
                        <div className="follow">
                            <p>Follow Us On:
                            </p>
                        </div>
                        <div className="footerIcon">
                            <a className="footerIconContent">
                                <i className="fab fa-facebook-f"></i>
                            </a>
                            <a className="footerIconContent">
                                <i className="fab fa-twitter"></i>
                            </a>
                            <a className="footerIconContent">
                                <i className="fab fa-google-plus-g"></i>
                            </a>
                            <a className="footerIconContent">
                                <i className="fab fa-linkedin-in"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div className="dNone" id="dNone">
                <Link id="bottomFixed"  to="header" smooth={true} duration={1600}>
                    <i className="fas fa-arrow-up"></i>
                </Link>
            </div>
        </div>
    </div>
}
export default Footer