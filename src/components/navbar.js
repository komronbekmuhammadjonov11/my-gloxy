import {Link} from "react-scroll";
import {useState} from "react";
function Navbar() {
    const [click, setclick] = useState(false);
    function navbarButton(){
        setclick(!click);
    }

    const style={
        marginLeft:"-200%",
        transitionDuration:"0.6s",


    }
    const style1={
        marginLeft:"0",
        transitionDuration:"0.6s",
    }
    return <div className="navbarNavbar" id="navbar">
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12">
                    <div className="navbar navbar-expand-md  navbar-light">
                        <div className="navbar-brand ">
                            <Link to="header" smooth={true}  duration={1200} className="nav-link" >
                                Glo
                                <span>xy</span>
                                <b>.</b>
                            </Link>
                        </div>
                        <button  className="navbarIcon" onClick={navbarButton}>
                            {click===false? <i className="fas fa-bars"></i> : <i className="fas fa-times"></i>}
                        </button>
                        <ul className="navbar-nav" style={click===false?style:style1}>
                            <li className="nav-item" >
                                <Link  className="nav-link " onClick={()=>setclick(false)}  smooth={true}  duration={1200} to="temple" >Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" smooth={true} onClick={()=>setclick(false)}  duration={700} to="demo">Demo </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" smooth={true} onClick={()=>setclick(false)} duration={1000} to="features" >Features</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="developer" onClick={()=>setclick(false)} smooth={true} duration={1300}  >Team</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="contactBottom" onClick={()=>setclick(false)} smooth={true} duration={1600}  >Contact</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link">
                                    <button className="btn btn-danger">
                                        <i className="fas fa-shopping-cart"></i>
                                        Purchase Now!
                                    </button>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default Navbar