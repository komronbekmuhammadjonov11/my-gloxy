import "../style/contact.scss"

function Contact() {
    return <div className="contact" id="contactBottom">
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <h1>Our Contact</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, enim eum in necessitatibus
                        perferendis rerum?</p>
                </div>
            </div>
            <div className="row">
                <div className="col-md-4">
                    <div className="address">
                        <div className="addressLeft">
                            <div className="addressIconContent">
                                <i className="fas fa-map-marker-alt"></i>
                            </div>
                        </div>
                        <div className="addressRight">
                            <h6>Address</h6>
                            <p>2750 Quadra Street Victoria,<br/> Canada.</p>
                        </div>
                    </div>
                    <div className="address">
                        <div className="addressLeft">
                            <div className="addressIconContent">
                                <i className="fas fa-envelope"></i>
                            </div>
                        </div>
                        <div className="addressRight">
                            <h6>Email</h6>
                            <p>hello@gloxy.com</p>
                        </div>
                    </div>
                    <div className="address">
                        <div className="addressLeft">
                            <div className="addressIconContent">
                                <i className="fas fa-phone-alt"></i>
                            </div>
                        </div>
                        <div className="addressRight">
                            <h6>Phone</h6>
                            <p>+1-325-555-0156</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-8">
                    <form action="#">
                        <div className="row">
                            <div className="col-md-6">
                                <input type="text" className="form-control" placeholder="Name"/>
                            </div>
                            <div className="col-md-6">
                                <input type="email" className="form-control" placeholder="Email"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <input type="text" className="form-control" placeholder="Subject"/>
                            </div>
                            <div className="col-md-6">
                                <input type="number" className="form-control" placeholder="Phone"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <textarea type="text" className="form-control " placeholder="Your Message"></textarea>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <button className="btn btn-warning ">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
}

export default Contact